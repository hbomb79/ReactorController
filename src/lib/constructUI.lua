local TARGETS = {
    reactor = "populateSetupReactors"
}

if client.applicationMode == "setup" then
    client:query "Button#cancelSetup":on( "trigger", function()
        client:stop()
    end )

    client:query "Button#finishSetup":on( "trigger", function()
        client:configureMasterUI()
    end )

    client:query "Button.setup-move":on( "trigger", function( self )
        if TARGETS[ self.setupNext ] then
            client[ TARGETS[ self.setupNext ] ]( client )
        end

        client.cache.masterPaginator:selectPage( self.setupNext )
    end )

    -- client:off( "" )
elseif client.applicationMode == "master" then
    client:query "Button#cancelSetup":on( "trigger", function()
        client.cache.masterPaginator:selectPage "welcome"
    end )

    client:query "Button#openConfig":on( "trigger", function()
        client.cache.masterPaginator:selectPage "reactor"
    end )

    client:query ".setup-only":remove()

    -- client:on(  )
else
    error("Unknown applicationMode " .. tostring( client.applicationMode ))
end

client:query "Button.help_toggle":on( "trigger", function( self )
    local parent = client.cache.masterPaginator:query( "Page#" .. self.helpTarget ).result[ 1 ]
    local help = parent:query ".help".result[ 1 ]
    help:setClass( "shown", not help:hasClass "shown" )

    local shown = help:hasClass "shown"
    help:animate( self.helpTarget .. "_help_toggle", "X", ( shown and parent.width - help.width or parent.width ) + 1, 0.15, "outSine" )
end )
