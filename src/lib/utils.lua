print "Loading classes... Please wait"

utils = {
    isInTable = function( table, key, value )
        if value then
            if table[ key ] == value then return true end
        else
            for i = 1, #table do
                if table[ i ] == key then return true, i end
            end
        end
    end,

    validatePeripheral = function( address, pType, err )
        local addressType = peripheral.getType( address )
        if type( pType ) == "table" then
            for i = 1, #pType do
                if addressType == pType[ 1 ] then
                    return true
                end
            end

            return err and error( err .. " -- Peripheral '"..address.."' not found or invalid type", 2 )
        else
            return ( addressType == pType ) or ( err and error( err .. " -- Peripheral '"..address.."' not found or invalid type", 2 ) )
        end
    end,

    testForPeripheralCategory = function( peripherals, cat )
        for address, peripheral in pairs( peripherals ) do
            if peripheral.category == cat then
                return true
            end
        end

        return false
    end
}
