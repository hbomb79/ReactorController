--[[
    Reactor Controller
    ==================

    A Titanium program to monitor and control different reactor systems in Minecraft.

    Supports:
    ---------
        - Mekanism Fusion Reactor
        - BigReactors Reactor (passively cooled)

    There are plans to support actively cooled Big Reactors (with turbines) in the near future.

    ** Copyright (c) Harry Felton 2017 **
]]

ReactorController.registerPeripheral( "Reactor Logic Adapter", FusionReactor )
ReactorController.registerPeripheral( "BigReactors-Reactor", BigReactor )

_G.client = ReactorController():set( "terminatable", true )
client:start()
